/**
 * @ngdoc service
 * @name todoService
 * @description get todo List from data source
 */
angular.module('rachidApp.services', ['ngResource'])
    .factory("todoService", function($resource) {
        return {
            "users": $resource('http://jsonplaceholder.typicode.com/users'),
            "list": $resource('http://jsonplaceholder.typicode.com/todos')
        };
});
