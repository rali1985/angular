'use strict';
angular.module('rachidApp.todo', ['ngRoute', 'ngResource', 'checklist-model', 'rachidApp.services'])
    .config(['$routeProvider', function($routeProvider) {
        $routeProvider.when('/todo', {
            templateUrl: 'todo/todo.html',
            controller: 'TodoController'
        });
    }])
    .controller('TodoController', ["$scope", "$resource", "$filter", "todoService", function($scope, $resource, $filter, todoService) {
        todoService.users.query(function(data) {
            $scope.users = data;
            $scope.loading = true;
            todoService.list.query(function(data) {
                $scope.loading = false;
                $scope.list = data
            });
        });
        $scope.selected = [];
        $scope.getUser = function(id) {
            var user = $filter('filter')($scope.users, {
                id: id
            })[0];
            return user['name'];
        }
        $scope.clear = function() {
            angular.forEach($scope.selected, function(id) {
                var item = $filter('filter')($scope.list, {
                    id: id
                })[0];
                item.completed = true;
            });
        }
        $scope.complete = function(id) {
            var item = $filter('filter')($scope.list, {
                id: id
            })[0];
            item.completed = true;
        }
    }]);
